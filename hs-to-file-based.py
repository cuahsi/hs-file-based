import datetime
import json
import os
import shutil
import uuid
from getpass import getpass

import tempfile
import zipfile
from hsclient import HydroShare


def parse_aggregations_and_files(agg, agg_dict, files_dict, id):
    md = agg.metadata.dict()

    files = {}
    for file in agg.files():
        file_id = str(uuid.uuid4())
        files[file_id] = {"path": file.path, "checksum": file.checksum}
    md["files"] = list(files.keys())
    files_dict.update(files)

    aggs = []
    for agg in agg.aggregations():
        uid = str(uuid.uuid4())
        aggs.append(uid)
        agg_dict, files = parse_aggregations_and_files(agg, agg_dict, files_dict, uid)

    md["aggregations"] = list(aggs)
    agg_dict[id] = md
    return agg_dict, files_dict


class HS(object):

    def __init__(self):
        username = input("Username: ").strip()
        password = getpass("Password for {}: ".format(username))
        self.hs = HydroShare(username=username, password=password)

    def retrieve(self):
        resource_id = input("Resource id: ").strip()
        res = self.hs.resource(resource_id)

        # retrieve resource files and unpack
        with tempfile.TemporaryDirectory() as tmp:
            print(f"Downloading {resource_id} ...")
            res.download(save_path=tmp)
            print("Extracting files...")
            with zipfile.ZipFile(os.path.join(tmp, f"{resource_id}.zip"), 'r') as zip_ref:
                zip_ref.extractall(tmp)
            shutil.move(os.path.join(tmp, resource_id, "data", "contents"), os.path.join(tmp, "contents_dir"))
            shutil.rmtree(os.path.join(tmp, resource_id))
            shutil.move(os.path.join(tmp, "contents_dir"), resource_id)

        print("Parsing files and metadata")
        aggregations, files = parse_aggregations_and_files(res, {}, {}, resource_id)

        # create .hs folder
        os.mkdir(os.path.join(resource_id, ".hs"))
        print("Writing .hs/aggregations.json")
        with open(os.path.join(resource_id, ".hs", "aggregations.json"), "w") as f:
            f.write(json.dumps(aggregations,
                               default=lambda o: o.__str__() if isinstance(o, datetime.datetime) else None,
                               indent=2))
        print("Writing .hs/files.json")
        with open(os.path.join(resource_id, ".hs", "files.json"), "w") as f:
            f.write(json.dumps(files, indent=2))


hs = HS()
hs.retrieve()
